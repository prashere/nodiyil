`nodi-yil` is a **Tamil** word meaning quickly. Using nodiyil you can quickly bootstrap or generate skeleton for most commonly used programming languages.

### Languages Supported
- python
- java
- c / c++
- ruby
- html

### Install
- clone (or) download this repository
- run the `install` script

        ./install

### Usage

        nodiyil language file_name

### Example

        nodiyil python test

The above example will create a test.py file in the location wherever you call it.

### Uninstall
- run the `uninstall` script

        ./uninstall

### Updating
- run the `update` script

        ./update

Note: **git** is required to update, since updating is a simple git pull away. Hope git is more common among developers and GNU/Linux users.

### License

nodiyil is licensed under GPLv3 which is a **free software** license. It simply means as a user and developer you have the following freedoms.

- to run this program for any purpose anywhere.
- to study and modify this source code to fit for your requirements and purposes.
- to distribute and redistribute both modifed and unmodified source code.

**GPL** enforces one thing, that is, you should not take away any of these freedom that is given to you when you pass on the modified or unmodified version of your's. Copyleft 2017.
