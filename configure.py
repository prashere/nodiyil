#!/usr/bin/env python3
from json import dumps
from os import path, remove


def is_config_file_present(file_path):
    return path.isfile(file_path)


def get_user_input(message):
    return input(message)


def bake_configuration(author, license):
    _config = {
        "author": author,
        "license": license,
        "language": {
            "py": "base.py",
            "rb": "base.rb",
            "java": "base.java",
            "c": "base.c",
            "cc": "base.cc",
            "html": "base.html"
        }
    }
    return _config


def write_config_to_file(config, file_path):
    with open(file_path, 'w') as fd:
        fd.writelines(dumps(config, indent=2))


def create_config_file(file_path):
    author_name = get_user_input("Your Name: ")
    license = get_user_input("license: ")
    configuration = bake_configuration(author_name, license)
    write_config_to_file(configuration, file_path)
    print("{} configuration file created".format(file_path))


if __name__ == "__main__":
    FILE_NAME = "skeleton.json"
    try:
        print("checking for {} configuration file".format(FILE_NAME))
        if is_config_file_present(FILE_NAME):
            print("configuration file exist")
        else:
            print("not found. Let's create one.")
            create_config_file(FILE_NAME)
    except Exception as e:
        print("Exception occured: {}".format(ex))
        if path.isfile(FILE_NAME):
            remove(FILE_NAME)
