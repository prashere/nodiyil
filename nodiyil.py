#!/usr/bin/env python
'''
Author: Prasanna Venkadesh
License: GNU GPL version 3

This software is licensed under a free software license,
which means you have the freedom

    - to run the program for any purpose.
    - to study and modify the source code.
    - to distribute copies of your modified version.
    - to redistribute copies of the same.
'''

# generic imports
import sys
import json
from os import path, environ


config_path = environ.get('HOME')


def create_file(fname, ext):
    full_path = path.abspath('.')
    with open(full_path+'/'+fname+'.%s' %ext, 'w') as outfile:
        with open(config_path+'/.nodiyil/skeleton/skeleton.json', 'r') as config:
            conf = json.loads(config.read())
        if ext == "py":
            outfile.writelines("#!/usr/bin/env python\n")
            outfile.writelines("'''\n")
            outfile.writelines("Author: %s\n" %conf['author'])
            outfile.writelines("License: %s\n" %conf['license'])
            outfile.writelines("'''\n\n")
        elif ext == "rb":
            outfile.writelines("#!/usr/bin/env ruby\n")
            outfile.writelines("=begin\n")
            outfile.writelines("Author: %s\n" %conf['author'])
            outfile.writelines("License: %s\n" %conf['license'])
            outfile.writelines("=end\n\n")
        elif ext == "c" or ext == "cc" or ext == "java":
            outfile.writelines("/*\n * Author: %s\n * License: %s\n */\n\n"
                    %(conf['author'], conf['license']))
            with open('Makefile', 'w') as makefile:
                if ext == "c":
                    makefile.writelines("CFLAGS=-Wall -g\n\n")
                    makefile.writelines("all:\n\tgcc %s.c -o %s\n" %(fname,fname))
                    makefile.writelines("clean:\n\trm -f %s" %fname)
                elif ext == "cc":
                    makefile.writelines("all:\n\tg++ %s.cc -o %s\n" %(fname, fname))
                    makefile.writelines("clean:\n\trm -f %s" %fname)
                elif ext == "java":
                    outfile.writelines("public class %s {\n" %fname.title())
                    makefile.writelines("all:\n\tjavac %s.java\n" %fname)
                    makefile.writelines("clean:\n\trm -f %s.class" %fname)
        elif ext == "html":
            outfile.writelines("<!--\n\tAuthor: %s\n\tLicense: %s\n-->\n\n" %(conf['author'], conf['license']))
        else:
            print ("No idea")
            exit()
        with open(config_path+"/.nodiyil/skeleton/"+conf['language'][ext], 'r') as base_file:
            outfile.writelines(base_file.readlines())
        print ("\nboilerplate code for %s.%s is created\n" %(fname, ext))


def main():
    # check system arguments count
    if len(sys.argv) > 2:
        program = sys.argv[1].lower()
        filename = sys.argv[2]
        if program == "python":
            create_file(filename, "py")
        elif program == "ruby":
            create_file(filename, "rb")
        elif program == "java":
            create_file(filename, "java")
        elif program == "c":
            create_file(filename, "c")
        elif program == "cpp":
            create_file(filename, "cc")
        elif program == "html":
            create_file(filename, "html")
        else:
            print ("\nI have no support for %s" %program)
            print ("If you think, I should support %s, then create" %program)
            print ("a issue in the project repository\n")
            exit()
    else:
        program_name = sys.argv[0].split('/')[-1]
        print ("\nUsage: %s language filename" %program_name)
        print ("Example: %s python hello_world\n" %program_name)
        exit()


if __name__ == '__main__':
    main()
